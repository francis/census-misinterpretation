# Census Misinterpretation

This code tests the ability to infer race and ethnicity given knowledge of a person's address for the original 2010 US Census release, and the 2010 release protected with the Bureau's improved privacy protection, Disclosure Avoidance System (DAS).

The code works with microdata (one row per Census respondent).

## Data Files

The census data is taken from IPUM NHGIS Privacy-Protected 2010 Census Demonstration Data. These are tables prepared by NHGIS containing the 2010 census data protected in the traditional way (swapping), and the same data protected by the Census Bureau's new Top-Down Algorithm (TDA).

The block-level data is released state-by-state, and can be found at https://www.nhgis.org/privacy-protected-2010-census-demonstration-data#v20210608-files.

We have copied the files into the directory `data`.

## Code Files

All the code resides in the file `compare.py`. It reads in the data files, runs the inference against the two types of data, and produces the output file `summary.json` as well as several plots in directory `plots`.

import os
import zipfile
import shutil
import json
import math
import matplotlib.pyplot as plt
from itertools import accumulate
from statistics import mean,stdev,pstdev
import pandas as pd
import pprint

pp = pprint.PrettyPrinter(indent=4)
numStates = 1000
bins = [[1,9],[10,49],[50,99],[100,249],[250,499],[500,1000000]]

SF = 0
DP = 1
totalPop = ['H74001_sf','H74001_dp']
notHispanicRaceCodes = [
        ['H75005_sf', 'H75006_sf', 'H75007_sf', 'H75008_sf', 'H75009_sf', 'H75010_sf', 'H75013_sf', 'H75014_sf', 'H75015_sf', 'H75016_sf', 'H75017_sf',
        'H75018_sf', 'H75019_sf', 'H75020_sf', 'H75021_sf', 'H75022_sf', 'H75023_sf', 'H75024_sf', 'H75025_sf', 'H75026_sf', 'H75027_sf', 'H75029_sf',
        'H75030_sf', 'H75031_sf', 'H75032_sf', 'H75033_sf', 'H75034_sf', 'H75035_sf', 'H75036_sf', 'H75037_sf', 'H75038_sf', 'H75039_sf', 'H75040_sf',
        'H75041_sf', 'H75042_sf', 'H75043_sf', 'H75044_sf', 'H75045_sf', 'H75046_sf', 'H75047_sf', 'H75048_sf', 'H75050_sf', 'H75051_sf', 'H75052_sf',
        'H75053_sf', 'H75054_sf', 'H75055_sf', 'H75056_sf', 'H75057_sf', 'H75058_sf', 'H75059_sf', 'H75060_sf', 'H75061_sf', 'H75062_sf', 'H75063_sf',
        'H75064_sf', 'H75066_sf', 'H75067_sf', 'H75068_sf', 'H75069_sf', 'H75070_sf', 'H75071_sf', 'H75073_sf',],
        [ 'H75005_dp', 'H75006_dp', 'H75007_dp', 'H75008_dp', 'H75009_dp', 'H75010_dp', 'H75013_dp', 'H75014_dp', 'H75015_dp', 'H75016_dp', 'H75017_dp',
        'H75018_dp', 'H75019_dp', 'H75020_dp', 'H75021_dp', 'H75022_dp', 'H75023_dp', 'H75024_dp', 'H75025_dp', 'H75026_dp', 'H75027_dp', 'H75029_dp',
        'H75030_dp', 'H75031_dp', 'H75032_dp', 'H75033_dp', 'H75034_dp', 'H75035_dp', 'H75036_dp', 'H75037_dp', 'H75038_dp', 'H75039_dp', 'H75040_dp',
        'H75041_dp', 'H75042_dp', 'H75043_dp', 'H75044_dp', 'H75045_dp', 'H75046_dp', 'H75047_dp', 'H75048_dp', 'H75050_dp', 'H75051_dp', 'H75052_dp',
        'H75053_dp', 'H75054_dp', 'H75055_dp', 'H75056_dp', 'H75057_dp', 'H75058_dp', 'H75059_dp', 'H75060_dp', 'H75061_dp', 'H75062_dp', 'H75063_dp',
        'H75064_dp', 'H75066_dp', 'H75067_dp', 'H75068_dp', 'H75069_dp', 'H75070_dp', 'H75071_dp', 'H75073_dp',]
]

allRaceCodes = [
      [ 'H74003_sf', 'H74004_sf', 'H74005_sf', 'H74006_sf', 'H74007_sf', 'H74008_sf', 'H74011_sf', 'H74012_sf', 'H74013_sf',
        'H74014_sf', 'H74015_sf', 'H74016_sf', 'H74017_sf', 'H74018_sf', 'H74019_sf', 'H74020_sf', 'H74021_sf', 'H74022_sf', 'H74023_sf',
        'H74024_sf', 'H74025_sf', 'H74027_sf', 'H74028_sf', 'H74029_sf', 'H74030_sf', 'H74031_sf', 'H74032_sf', 'H74033_sf', 'H74034_sf',
        'H74035_sf', 'H74036_sf', 'H74037_sf', 'H74038_sf', 'H74039_sf', 'H74040_sf', 'H74041_sf', 'H74042_sf', 'H74043_sf', 'H74044_sf',
        'H74045_sf', 'H74046_sf', 'H74048_sf', 'H74049_sf', 'H74050_sf', 'H74051_sf', 'H74052_sf', 'H74053_sf', 'H74054_sf', 'H74055_sf',
        'H74056_sf', 'H74057_sf', 'H74058_sf', 'H74059_sf', 'H74060_sf', 'H74061_sf', 'H74062_sf', 'H74064_sf', 'H74065_sf', 'H74066_sf',
        'H74067_sf', 'H74068_sf', 'H74069_sf', 'H74071_sf',],
      [ 'H74003_dp', 'H74004_dp', 'H74005_dp', 'H74006_dp', 'H74007_dp', 'H74008_dp', 'H74011_dp', 'H74012_dp', 'H74013_dp',
        'H74014_dp', 'H74015_dp', 'H74016_dp', 'H74017_dp', 'H74018_dp', 'H74019_dp', 'H74020_dp', 'H74021_dp', 'H74022_dp', 'H74023_dp',
        'H74024_dp', 'H74025_dp', 'H74027_dp', 'H74028_dp', 'H74029_dp', 'H74030_dp', 'H74031_dp', 'H74032_dp', 'H74033_dp', 'H74034_dp',
        'H74035_dp', 'H74036_dp', 'H74037_dp', 'H74038_dp', 'H74039_dp', 'H74040_dp', 'H74041_dp', 'H74042_dp', 'H74043_dp', 'H74044_dp',
        'H74045_dp', 'H74046_dp', 'H74048_dp', 'H74049_dp', 'H74050_dp', 'H74051_dp', 'H74052_dp', 'H74053_dp', 'H74054_dp', 'H74055_dp',
        'H74056_dp', 'H74057_dp', 'H74058_dp', 'H74059_dp', 'H74060_dp', 'H74061_dp', 'H74062_dp', 'H74064_dp', 'H74065_dp', 'H74066_dp',
        'H74067_dp', 'H74068_dp', 'H74069_dp', 'H74071_dp',]
]

def getMajorityRace(dat,i,X):
    maxCnt = 0
    maxRace = None
    maxRaceIndex = None
    maxHispanic = None
    for raceIndex in range(len(notHispanicRaceCodes[X])):
        # Need to determine if the hispanic race or not hispanic race
        # is the larger
        nhCol = notHispanicRaceCodes[X][raceIndex]
        allCol = allRaceCodes[X][raceIndex]
        nhNum = dat[nhCol][i]
        allNum = dat[allCol][i]
        hNum = allNum - nhNum
        if hNum < 0:
            print(f"bad hNum (i={i}, X={X}, raceIndex={raceIndex}")
            quit()
        if nhNum > hNum:
            num = nhNum
            race = nhCol
            hispanic = False
        else:
            num = hNum
            race = allCol
            hispanic = True
        if num > maxCnt:
            maxCnt = num
            maxRace = race
            maxRaceIndex = raceIndex
            maxHispanic = hispanic
    return({'race':maxRace,'hispanic':maxHispanic,'count':maxCnt,'raceIndex':maxRaceIndex})

def getGroundTruthRaceCount(dat,i,majRace):
    raceIndexDP = majRace[DP]['raceIndex']
    if raceIndexDP is None:
        pp.pprint(f"Error getGroundTruthRaceCount: {majRace}")
        quit()
    if majRace[DP]['hispanic']:
        raceSF = allRaceCodes[SF][raceIndexDP]
        raceSFNot = notHispanicRaceCodes[SF][raceIndexDP]
        count = dat[raceSF][i] - dat[raceSFNot][i]
    else:
        raceSF = notHispanicRaceCodes[SF][raceIndexDP]
        count = dat[raceSF][i]
    return(count,raceSF)

def getBlockResults(dat,i):
    majRace = [None,None]
    precision = [None,None]
    majRace[SF] = getMajorityRace(dat,i,SF)
    majRace[DP] = getMajorityRace(dat,i,DP)
    majRace[SF]['total'] = dat[totalPop[SF]][i]
    majRace[DP]['total'] = dat[totalPop[DP]][i]
    precision[SF] = 100 * (majRace[SF]['count'] / dat[totalPop[SF]][i])
    gtMajCount,gtRace = getGroundTruthRaceCount(dat,i,majRace)
    majRace[DP]['gtMajCount'] = gtMajCount
    majRace[DP]['gtRace'] = gtRace
    majRace[DP]['error'] = majRace[DP]['count'] - gtMajCount
    precision[DP] = 100 * (gtMajCount / dat[totalPop[SF]][i])
    return({'maj':majRace,'precision':precision})

def initTally():
    tally = {}
    tally['precAll'] = [[],[]]
    tally['errAll'] = []
    tally['gtMajCount'] = [[],[]]
    tally['totalAll'] = [[],[]]
    tally['precBin'] = [[[] for i in range(len(bins))],[[] for i in range(len(bins))]]
    tally['errBin'] = [[] for i in range(len(bins))]
    tally['totalBin'] = [[[] for i in range(len(bins))],[[] for i in range(len(bins))]]
    return tally

def getBinIndex(res):
    minTotal = res['maj'][SF]['total']
    for binIndex in range(len(bins)):
        if bins[binIndex][1] >= minTotal:
            return binIndex

def tallyData(filePath,tally):
    if filePath[-3:] != 'zip':
        print(f"bad file path {filePath}")
        quit()
    # unzip into temp directory
    if os.path.exists('temp'):
        shutil.rmtree('temp')
    os.mkdir('temp')
    with zipfile.ZipFile(filePath,"r") as zip_ref:
        zip_ref.extractall('temp')
    # find and read csv file
    for fileName in os.listdir('temp'):
        if fileName[-3:] == 'csv':
            print(f"Processing {fileName}")
            df = pd.read_csv(os.path.join('temp',fileName))
            dat = df.to_dict(orient='list')
    # Loop through every block and tally results
    for i in range(len(dat['H72001_sf'])):
        if dat[totalPop[SF]][i] == 0 or dat[totalPop[DP]][i] == 0:
            continue
        res = getBlockResults(dat,i)
        #if (res['precision'][DP]*res['maj'][DP]['total']) > (res['precision'][SF]*res['maj'][SF]['total']):
            #pp.pprint(res)
        binIndex = getBinIndex(res)
        #print(f"binIndex {binIndex}")
        #print('-----')
        tally['precAll'][SF].append(res['precision'][SF])
        tally['precAll'][DP].append(res['precision'][DP])
        # The following two are the same value, but we need both because we'll
        # sort with these later
        tally['gtMajCount'][DP].append(res['maj'][DP]['gtMajCount'])
        tally['gtMajCount'][SF].append(res['maj'][DP]['gtMajCount'])
        tally['errAll'].append(res['maj'][DP]['error'])
        tally['totalAll'][SF].append(res['maj'][SF]['total'])
        # The following assignment of SF total to DP total is intentional. We do it
        # because we use the SF value to assign precision (ground truth) to a DP
        # prediction, so to be consistent we do the same here.
        tally['totalAll'][DP].append(res['maj'][SF]['total'])
        tally['precBin'][SF][binIndex].append(res['precision'][SF])
        tally['precBin'][DP][binIndex].append(res['precision'][DP])
        tally['errBin'][binIndex].append(res['maj'][DP]['error'])
        tally['totalBin'][SF][binIndex].append(res['maj'][SF]['total'])
        # Same thing here
        tally['totalBin'][DP][binIndex].append(res['maj'][SF]['total'])
    shutil.rmtree('temp')
    return tally

def summarizeTally(tally):
    summary = {}
    if len(tally['precAll'][SF]) != len(tally['totalAll'][SF]):
        print(f"Bad lens SF")
        quit()
    if len(tally['precAll'][DP]) != len(tally['totalAll'][DP]):
        print(f"Bad lens DP")
        quit()
    summary['precAllPersons'] = {'SF':{},'DP':{}}
    m,sd,n = getStats(tally['precAll'][SF],tally['totalAll'][SF])
    summary['precAllPersons']['SF']['mean'] = m
    summary['precAllPersons']['SF']['stdev'] = sd
    summary['precAllPersons']['SF']['numBlocks'] = len(tally['totalAll'][SF])
    m,sd,n = getStats(tally['precAll'][DP],tally['totalAll'][DP])
    summary['precAllPersons']['DP']['mean'] = m
    summary['precAllPersons']['DP']['stdev'] = sd
    summary['precAllPersons']['DP']['numBlocks'] = len(tally['totalAll'][DP])
    summary['totalAll'] = {'SF':sum(tally['totalAll'][SF]),
                           'DP':sum(tally['totalAll'][DP])}
    summary['errAll'] = {}
    m,sd,n = getStats(tally['errAll'],tally['totalAll'][DP])
    summary['errAll']['mean'] = m
    summary['errAll']['stdev'] = sd
    summary['errAll']['numBlocks'] = len(tally['totalAll'][DP])
    summary['byBin'] = {'labels':[],
                        'numBlocks':[],
                        'total':{'SF':[],'DP':[]},
                        'precision':{'SF':{'mean':[],'stdev':[]},
                                     'DP':{'mean':[],'stdev':[]}},
                        'error':{'mean':[],'stdev':[]},
    }
    for binIndex in range(len(bins)):
        bin = str(bins[binIndex][0])
        summary['byBin']['labels'].append(bin)
        summary[bin] = {}
        summary[bin]['precBin'] = {'SF':{},'DP':{}}
        m,sd,n = getStats(tally['precBin'][SF][binIndex],tally['totalBin'][SF][binIndex])
        summary[bin]['precBin']['SF']['mean'] = m
        summary['byBin']['precision']['SF']['mean'].append(m)
        summary[bin]['precBin']['SF']['stdev'] = sd
        summary['byBin']['precision']['SF']['stdev'].append(sd)
        summary[bin]['precBin']['SF']['numBlocks'] = len(tally['totalBin'][SF][binIndex])
        summary['byBin']['numBlocks'].append(n)

        m,sd,n = getStats(tally['precBin'][DP][binIndex],tally['totalBin'][DP][binIndex])
        summary[bin]['precBin']['DP']['mean'] = m
        summary['byBin']['precision']['DP']['mean'].append(m)
        summary[bin]['precBin']['DP']['stdev'] = sd
        summary['byBin']['precision']['DP']['stdev'].append(sd)
        summary[bin]['precBin']['DP']['numBlocks'] = len(tally['totalBin'][DP][binIndex])

        summary[bin]['totalBin'] = {'SF':sum(tally['totalBin'][SF][binIndex]),
                            'DP':sum(tally['totalBin'][DP][binIndex])}
        summary['byBin']['total']['SF'].append(sum(tally['totalBin'][SF][binIndex]))
        summary['byBin']['total']['DP'].append(sum(tally['totalBin'][DP][binIndex]))

        m,sd,n = getStats(tally['errBin'][binIndex],tally['totalBin'][DP][binIndex])
        summary[bin]['errBin'] = {}
        summary[bin]['errBin']['mean'] = m
        summary['byBin']['error']['mean'].append(m)
        summary[bin]['errBin']['stdev'] = sd
        summary['byBin']['error']['stdev'].append(sd)
        summary[bin]['errBin']['numBlocks'] = len(tally['totalBin'][DP][binIndex])
    # Compute number of persons with 100% and >95% precision
    summary['95th'] = {}
    summary['100th'] = {}
    cnt95,cnt100,total = get95th100th(tally['precAll'][SF],tally['totalAll'][SF])
    print(cnt95,cnt100,total)
    summary['95th']['SF'] = {'persons':cnt95,'percent':round(100*(cnt95/total),2)}
    summary['100th']['SF'] = {'persons':cnt100,'percent':round(100*(cnt100/total),2)}
    cnt95,cnt100,total = get95th100th(tally['precAll'][DP],tally['totalAll'][DP])
    print(cnt95,cnt100,total)
    summary['95th']['DP'] = {'persons':cnt95,'percent':round(100*(cnt95/total),2)}
    summary['100th']['DP'] = {'persons':cnt100,'percent':round(100*(cnt100/total),2)}
    # Sort precision for later plotting. (Sort total along side to keep in sync)
    # Do the same for different thresholds of the majority race count
    totAll = summary['totalAll']['DP']
    summary['byMajorityCount'] = {'recall':{},'precision':{}, '95th':{}, '100th':{}, 'fail':{},}
    for thresh in [0,2,5,10,20,50]:
        precision,all,num95,num100,fails = get95th100thByMajorityCount(tally,DP,thresh)
        summary['byMajorityCount']['precision'][thresh] = round(precision,2)
        summary['byMajorityCount']['recall'][thresh] = round(100*(all/totAll),2)
        summary['byMajorityCount']['95th'][thresh] = num95
        summary['byMajorityCount']['100th'][thresh] = num100
        summary['byMajorityCount']['fail'][thresh] = fails
    tally['precAll'][SF], tally['totalAll'][SF], tally['gtMajCount'][SF] = (list(t) for t in zip(*sorted(zip(tally['precAll'][SF], tally['totalAll'][SF], tally['gtMajCount'][SF]))))
    tally['precAll'][DP], tally['totalAll'][DP], tally['gtMajCount'][DP] = (list(t) for t in zip(*sorted(zip(tally['precAll'][DP], tally['totalAll'][DP], tally['gtMajCount'][DP]))))
    return summary

def get95th100th(precs,counts):
    if len(precs) != len(counts):
        print(f"get95th100th error")
        quit()
    num100 = 0
    total = 0
    num95 = 0
    for prec,cnt in zip(precs,counts):
        total += cnt
        if prec > 99.9999:
            num100 += cnt
        if prec >+ 95:
            num95 += cnt
    return num95,num100,total

def getStats(tallyBins,tallyCounts):
    # I know I should be using numpy.  :(
    N = sum(tallyCounts)
    total = sum([b*c for b,c in zip(tallyBins,tallyCounts)])
    mean = total/N
    stdev = math.sqrt(sum([(((b-mean)**2)*c) for b,c in zip(tallyBins,tallyCounts)])/N)
    return mean,stdev,N

def filterByBlockSize(tally,sumXX,XX,threshold):
    bins = []
    for i in range(len(tally['precAll'][XX])):
        if tally['totalAll'][XX][i] >= threshold:
            bins.append(tally['precAll'][XX][i])
        else:
            # Below threshold, so assign precision of zero
            bins.append(0)
    bins, counts = (list(t) for t in zip(*sorted(zip(bins, tally['totalAll'][XX]))))
    pdf = [c/sumXX for c in counts]
    cum = list(accumulate(pdf))
    return bins, cum

def get95th100thByMajorityCount(tally,XX,threshold):
    all = 0
    avgTotal = 0
    avgCount = 0
    num100 = 0
    num95 = 0
    fails = 0
    for i in range(len(tally['precAll'][XX])):
        avgCount += tally['totalAll'][XX][i]
        if tally['gtMajCount'][XX][i] >= threshold:
            avgTotal += tally['precAll'][XX][i] * tally['totalAll'][XX][i]
            all += tally['totalAll'][XX][i]
            if tally['precAll'][XX][i] > 99.9999:
                num100 += tally['totalAll'][XX][i]
            if tally['precAll'][XX][i] >+ 95:
                num95 += tally['totalAll'][XX][i]
        else:
            if tally['precAll'][XX][i] > 80:
                fails += tally['totalAll'][XX][i]
    return avgTotal/avgCount,all,num95,num100,fails

def filterByMajorityCount(tally,sumXX,XX,threshold):
    bins = []
    for i in range(len(tally['precAll'][XX])):
        if tally['gtMajCount'][XX][i] >= threshold:
            bins.append(tally['precAll'][XX][i])
        else:
            # Below threshold, so assign precision of zero
            bins.append(0)
    bins, counts = (list(t) for t in zip(*sorted(zip(bins, tally['totalAll'][XX]))))
    pdf = [c/sumXX for c in counts]
    cum = list(accumulate(pdf))
    return bins, cum

def doPlots(tally,summary):
    binsBureau = [0,0,52.59,56.05,64.6,72.41,82,91.68,96.98,96.98]
    cumBureau = [0,0.343,0.343,0.367,0.414,0.497,0.661,0.825,0.985,1.00]
    binsSF = tally['precAll'][SF]
    countsSF = tally['totalAll'][SF]
    sumSF = summary['totalAll']['SF']
    pdfSF = [c/sumSF for c in countsSF]
    cumSF = list(accumulate(pdfSF))
    binsDP = tally['precAll'][DP]
    countsDP = tally['totalAll'][DP]
    sumDP = summary['totalAll']['DP']
    pdfDP = [c/sumDP for c in countsDP]
    cumDP = list(accumulate(pdfDP))
    filePath = os.path.join('plots','precision_cdf.png')
    plt.figure(figsize=(6, 3))
    plt.plot(binsBureau,cumBureau,'--',label="Bureau's re-identification on Swap")
    plt.plot(binsSF[1:],cumSF[1:],label='Simple Inference on Swap')
    plt.plot(binsDP[1:],cumDP[1:],label='Simple Inference on TDA')
    plt.ylabel('Cumulative Distribution',fontsize=12)
    plt.xlabel('Precision',fontsize=12)
    plt.legend(loc='upper left', ncol=1)
    plt.savefig(filePath,bbox_inches='tight')

    bins5DP, cum5DP = filterByMajorityCount(tally,sumDP,DP,5)
    bins5SF, cum5SF = filterByMajorityCount(tally,sumSF,SF,5)
    filePath = os.path.join('plots','precision_thresh5_cdf.png')
    plt.figure(figsize=(6, 3))
    plt.plot(binsBureau,cumBureau,'--',label="Bureau's re-identification on Swap")
    plt.plot(bins5SF[1:],cum5SF[1:],label='Simple Inference on Swap')
    plt.plot(bins5DP[1:],cum5DP[1:],label='Simple Inference on TDA')
    plt.ylabel('Cumulative Distribution',fontsize=12)
    plt.xlabel('Precision',fontsize=12)
    plt.legend(loc='upper left', ncol=1)
    plt.savefig(filePath,bbox_inches='tight')

#    bins10, cum10 = filterByBlockSize(tally,sumDP,DP,10)
#    bins20, cum20 = filterByBlockSize(tally,sumDP,DP,20)
#    bins50, cum50 = filterByBlockSize(tally,sumDP,DP,50)
#    bins100, cum100 = filterByBlockSize(tally,sumDP,DP,100)
#    bins200, cum200 = filterByBlockSize(tally,sumDP,DP,200)
#    filePath = os.path.join('plots','precision_cdf_by_size.png')
#    plt.figure(figsize=(6, 3))
#    plt.plot(bins10[1:],cum10[1:],label='>= 10')
#    plt.plot(bins20[1:],cum20[1:],label='>= 20')
#    plt.plot(bins50[1:],cum50[1:],label='>= 50')
#    plt.plot(bins100[1:],cum100[1:],label='>= 100')
#    plt.plot(bins200[1:],cum200[1:],label='>= 200')
#    plt.ylabel('Cumulative Distribution',fontsize=12)
#    plt.xlabel('Precision by Minimum Block Size',fontsize=12)
#    plt.legend(loc='lower right', ncol=1)
#    plt.savefig(filePath,bbox_inches='tight')
#
    bins2, cum2 = filterByMajorityCount(tally,sumDP,DP,2)
    bins5, cum5 = filterByMajorityCount(tally,sumDP,DP,5)
    bins10, cum10 = filterByMajorityCount(tally,sumDP,DP,10)
    bins20, cum20 = filterByMajorityCount(tally,sumDP,DP,20)
    filePath = os.path.join('plots','precision_cdf_by_maj_count.png')
    plt.figure(figsize=(6, 3))
    plt.plot(binsBureau,cumBureau,'--', label='Bureau')
    plt.plot(binsDP[1:],cumDP[1:],label='>= 0')
    plt.plot(bins2[1:],cum2[1:],label='>= 2')
    plt.plot(bins5[1:],cum5[1:],label='>= 5')
    plt.plot(bins10[1:],cum10[1:],label='>= 10')
    plt.plot(bins20[1:],cum20[1:],label='>= 20')
    plt.ylabel('Cumulative Distribution',fontsize=12)
    plt.xlabel('Precision',fontsize=12)
    plt.legend(loc='upper left', ncol=2)
    plt.savefig(filePath,bbox_inches='tight')

    # Let's look at the CDF, but filtered by the size of the block

    filePath = os.path.join('plots','error_per_bin.png')
    plt.figure(figsize=(6, 3))
    data = []
    xticks = []
    labels = []
    for i in range(len(bins)):
        data.append(tally['errBin'][i])
        xticks.append(i+1)
        labels.append(bins[i][0])
    plt.boxplot(data,showfliers=False)
    plt.xticks(xticks,labels)
    plt.xlabel('Block Size',fontsize=12)
    plt.ylabel('Absolute Error',fontsize=12)
    plt.savefig(filePath,bbox_inches='tight')

    filePath = os.path.join('plots','precision_per_bin.png')
    fig,(ax1,ax2) = plt.subplots(1,2,figsize=[6,2])
    data1 = []
    data2 = []
    xticks = []
    labels = []
    for i in range(len(bins)):
        data1.append(tally['precBin'][SF][i])
        data2.append(tally['precBin'][DP][i])
        xticks.append(i+1)
        labels.append(bins[i][0])
    ax1.boxplot(data1,showfliers=False)
    ax2.boxplot(data2,showfliers=False)
    ax1.set_xticklabels(labels)
    ax2.set_xticklabels(labels)
    ax1.set_ylabel('Precision',fontsize=12)
    ax1.set_xlabel('Block Size (Swap)',fontsize=12)
    ax2.set_xlabel('Block Size (TDA)',fontsize=12)
    ax1.set_ylim([0,100])
    ax2.set_ylim([0,100])
    plt.savefig(filePath,bbox_inches='tight')

stateCount = 0
tally = initTally()
for fileName in os.listdir("data"):
    tallyData(os.path.join('data',fileName),tally)
    stateCount += 1
    if stateCount >= numStates:
        break
print("Summarizing")
summary = summarizeTally(tally)
with open('summary.json', 'w') as outfile:
    json.dump(summary, outfile, indent=4)
print("Plotting")
doPlots(tally,summary)